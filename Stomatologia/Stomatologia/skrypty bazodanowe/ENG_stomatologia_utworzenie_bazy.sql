create database Stomatology;

use Stomatology;

create table Doctors
(
	DoctorId int identity not null,
	DoctorFirstName varchar(40) not null,
	DoctorLastName varchar(60) not null,
	DoctorAcademicTitle varchar(30) not null,
	constraint pk_doctors primary key (DoctorId)
);

create table DoctorsAvailability
(
	DoctorId int not null,
	DayOfTheWeek int not null,
	HourFrom int not null,
	HourTo int not null,
	constraint pk_doctors_availability primary key (DoctorId,DayOfTheWeek),
	constraint fk_doctors_availability_id foreign key(DoctorId) references Doctors(DoctorId) on delete cascade on update cascade
);

alter table DoctorsAvailability
add constraint ck_doctors_availability_day check ((DayOfTheWeek) between 1 and 5);

create table Patients
(
	PatientId int identity not null,
	PatientFirstName varchar(40) not null,
	PatientLastName varchar(60) not null,
	PatientPESEL varchar(11) not null,
	Email varchar(100) not null,
	PhoneNumber varchar(25) not null,
	constraint pk_patients primary key (PatientId)
);

alter table Patients
add constraint uq_patients_pesel unique(PatientPESEL);

create table Appointments
(
	AppointmentId int identity not null,
	AppointmentDateTime datetime not null,
	DoctorId int not null,
	IsAvailable bit not null,
	PatientId int null,
	constraint pk_appointments primary key (AppointmentId),
	constraint fk_appointments_doctors foreign key (DoctorId) references Doctors(DoctorId) on delete cascade on update cascade,
	constraint fk_appointments_patients foreign key (PatientId) references Patients(PatientId)
);