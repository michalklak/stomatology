use Stomatology;

INSERT INTO Doctors
            (DoctorFirstName, 
             DoctorLastName,
			 DoctorAcademicTitle) 
VALUES      ('Anna', 
             'Przykładowa', 
             'dr n. med.'), 
            ('Julia', 
             'Poglądowa', 
             'lek.'), 
            ('Cezary', 
             'Zwyczajny', 
             'dr hab. n. med.'), 
            ('Olga', 
             'Taka-Owaka', 
             'lek.'); 

INSERT INTO DoctorsAvailability
  VALUES
  -- Anna Przykładowa
  (1, 1, 10, 14),
  (1, 2, 12, 18),
  (1, 3, 10, 14),
  (1, 4, 8, 18),
  (1, 5, 10, 14),
  -- Julia Poglądowa
  (2, 1, 8, 16),
  (2, 2, 12, 18),
  (2, 3, 8, 16),
  (2, 5, 8, 16),
  -- Cezary Zwyczajny
  (3, 1, 12, 16),
  (3, 2, 12, 16),
  (3, 3, 8, 16),
  (3, 4, 8, 16),
  -- Olga Taka-Owaka
  (4, 1, 8, 18),
  (4, 2, 12, 16),
  (4, 3, 8, 18),
  (4, 4, 12, 16),
  (4, 5, 10, 18);