create database Stomatologia;

use Stomatologia;

create table Lekarze
(
	IdLekarza int identity not null,
	LekarzImie varchar(40) not null,
	LekarzNazwisko varchar(60) not null,
	LekarzTytul varchar(30) not null,
	constraint pk_lekarze primary key (IdLekarza)
);

create table LekarzeDostepnosc
(
	IdLekarza int not null,
	DzienTygodnia int not null,
	GodzinaOd int not null,
	GodzinaDo int not null,
	constraint pk_lekarze_dostepnosc primary key (IdLekarza,DzienTygodnia),
	constraint fk_lekarze_dostepnosc_id foreign key(IdLekarza) references Lekarze(IdLekarza) on delete cascade on update cascade
);

alter table LekarzeDostepnosc
add constraint ck_lekarze_dostepnosc_dzien check ((DzienTygodnia) between 1 and 5);

create table Pacjenci 
(
	IdPacjenta int identity not null,
	PacjentImie varchar(40) not null,
	PacjentNazwisko varchar(60) not null,
	PacjentPESEL varchar(11) not null,
	Email varchar(100) not null,
	NrTelefonu varchar(25) not null,
	constraint pk_pacjenci primary key (IdPacjenta)
);

alter table Pacjenci
add constraint uq_pacjenci_pesel unique(PacjentPESEL);

create table Wizyty
(
	IdWizyty int identity not null,
	Termin datetime not null,
	IdLekarza int not null,
	CzyDostepna bit not null,
	IdPacjenta int null,
	constraint pk_wizyty primary key (IdWizyty),
	constraint fk_wizyty_lekarze foreign key (IdLekarza) references Lekarze(IdLekarza) on delete cascade on update cascade,
	constraint fk_wizyty_pacjenci foreign key (IdPacjenta) references Pacjenci(IdPacjenta)
);