﻿using Microsoft.AspNet.Identity;
using Stomatologia.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Stomatologia.Controllers
{
    public class AppointmentsController : Controller
    {
        private StomatologyEntities dbState = new StomatologyEntities();

        private List<DateTime> freeDaysList = new List<DateTime>
        {
            new DateTime(2019,4,22),
            new DateTime(2019,5,1),
            new DateTime(2019,5,2),
            new DateTime(2019,5,3),
            new DateTime(2019,6,20),
            new DateTime(2019,8,15),
            new DateTime(2019,11,1),
            new DateTime(2019,11,11),
            new DateTime(2019,12,24),
            new DateTime(2019,12,25),
            new DateTime(2019,12,26)
        };

        private int patientId;

        [Authorize]
        public ActionResult Archive()
        {
            SetPatientIdIfNeeded();
            var appointments = dbState.Appointments.Where(w => w.PatientId == patientId).Where(w => w.IsAvailable == false).Where(s => s.AppointmentDateTime < DateTime.Now).Include(w => w.Doctors).Include(w => w.Patients);
            appointments = appointments.OrderByDescending(m => m.AppointmentDateTime);
            return View(appointments.ToList());
        }

        // GET: Appointments
        [Authorize]
        public ActionResult Index()
        {
            SetPatientIdIfNeeded();
            var Appointments = dbState.Appointments.Where(w => w.PatientId == patientId).Where(w => w.IsAvailable == false).Where(s => s.AppointmentDateTime >= DateTime.Now).Include(w => w.Doctors).Include(w => w.Patients).OrderBy(o => o.AppointmentDateTime);
            return View(Appointments.ToList());
        }

        // GET: Appointments/Create
        [Authorize]
        public ActionResult Create(int doctorId)
        {
            SetPatientIdIfNeeded();

            DateTime minDate = DateTime.Now.Date.AddDays(1);
            var availableAppointments = dbState.Appointments.Where(s => s.DoctorId == doctorId).Where(s => s.IsAvailable == true).Where(s => s.AppointmentDateTime >= minDate).Select(s => s.AppointmentDateTime);
            var availableAppointmentDates = new List<DateTime>();
            var availableAppointmentDatesAsString = new List<string>();
            foreach (var availableAppointment in availableAppointments)
            {
                if (!availableAppointmentDates.Contains(availableAppointment.Date))
                {
                    availableAppointmentDates.Add(availableAppointment.Date);
                    availableAppointmentDatesAsString.Add(string.Concat(availableAppointment.Month, "/", availableAppointment.Day, "/", availableAppointment.Year));
                }
            }
            NewAppointmentViewModel newViewModel = new NewAppointmentViewModel { DoctorId = doctorId, AvailableDatesString = availableAppointmentDatesAsString, AvailableHoursString = new List<string>() };
            return View(newViewModel);
        }

        // POST: Appointments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DoctorId,AppointmentDate,AppointmentTime")]NewAppointmentViewModel appointment)
        {
            // take date dirctly from view as string
            string dateFromView = Request.Form["DatePicker"];
            if (appointment.AppointmentDate == null || appointment.AppointmentDate < DateTime.Now)
            {
                // split date to get year, month, day
                string[] dateFromModelSplitted = dateFromView.Split('/');
                int yearFromView = Convert.ToInt32(dateFromModelSplitted[2]);
                int monthFromView = Convert.ToInt32(dateFromModelSplitted[0]);
                int dayFromView = Convert.ToInt32(dateFromModelSplitted[1]);

                // write date in model attribute
                DateTime dateToSet = new DateTime(yearFromView, monthFromView, dayFromView);
                appointment.AppointmentDate = dateToSet;
            }

            int doctorId = appointment.DoctorId;

            // if patient is logged and patientId has not beed saved until now, save it in private field
            SetPatientIdIfNeeded();

            // take hour directly from view as string
            if (appointment.AppointmentTime == null) appointment.AppointmentTime = Request.Form["hoursDropDownList"];

            // split time to hour and minutes
            string[] appointmentInTimeFormat = appointment.AppointmentTime.Split(':');
            double appointmentHour = Convert.ToDouble(appointmentInTimeFormat[0]);
            double appointmentMinutes = Convert.ToDouble(appointmentInTimeFormat[1]);

            // create date to find in database: add time to date created above
            DateTime appointmentToFind = appointment.AppointmentDate.AddHours(appointmentHour).AddMinutes(appointmentMinutes);

            // find the appointment in database
            var appointmentsFound = dbState.Appointments.Where(s => s.DoctorId == appointment.DoctorId).Where(s => s.AppointmentDateTime == appointmentToFind);

            // if no appointment is found, it means that user probably chose something wrong
            if (appointmentsFound.ToList().Count == 0)
            {
                // fill data that should be shown in view if model validation returns errors
                DateTime minDate = DateTime.Now.Date.AddDays(1);
                var availableAppointments = dbState.Appointments.Where(s => s.DoctorId == doctorId).Where(s => s.IsAvailable == true).Where(s => s.AppointmentDateTime >= minDate).Select(s => s.AppointmentDateTime);
                var availableAppointmentDates = new List<DateTime>();
                var availableAppointmentDatesAsString = new List<string>();
                foreach (var availableAppointment in availableAppointments)
                {
                    if (!availableAppointmentDates.Contains(availableAppointment.Date))
                    {
                        availableAppointmentDates.Add(availableAppointment.Date);
                        availableAppointmentDatesAsString.Add(string.Concat(availableAppointment.Month, "/", availableAppointment.Day, "/", availableAppointment.Year));
                    }
                }

                appointment.AvailableDatesString = availableAppointmentDatesAsString;
                appointment.AvailableHoursString = new List<string>();
                ModelState.AddModelError("", "Proszę zweryfikować poprawność wybranego terminu.");
                return View(appointment);
            }

            // if the logged patient has already booked an appointment in the same date and time, prevent it
            var appointmentsWithTheSameDatetime = dbState.Appointments.Where(s => s.AppointmentDateTime == appointmentToFind).Where(s => s.PatientId == patientId);
            if (appointmentsWithTheSameDatetime.Count() > 0)
            {
                // fill data that should be shown in view if model validation returns errors
                DateTime minDate = DateTime.Now.Date.AddDays(1);
                var availableAppointments = dbState.Appointments.Where(s => s.DoctorId == doctorId).Where(s => s.IsAvailable == true).Where(s => s.AppointmentDateTime >= minDate).Select(s => s.AppointmentDateTime);
                var availableAppointmentDates = new List<DateTime>();
                var availableAppointmentDatesAsString = new List<string>();
                foreach (var availableAppointment in availableAppointments)
                {
                    if (!availableAppointmentDates.Contains(availableAppointment.Date))
                    {
                        availableAppointmentDates.Add(availableAppointment.Date);
                        availableAppointmentDatesAsString.Add(string.Concat(availableAppointment.Month, "/", availableAppointment.Day, "/", availableAppointment.Year));
                    }
                }

                appointment.AvailableDatesString = availableAppointmentDatesAsString;
                appointment.AvailableHoursString = new List<string>();
                ModelState.AddModelError("", "W podanym terminie istnieje już zarezerwowana wizyta dla tego użytkownika. Proszę wybrać inny termin.");
                return View(appointment);
            }

            // take "first or default" that is available - we expect exact 1 result in database
            Appointments appointmentToAdd = appointmentsFound.Where(s => s.IsAvailable == true).FirstOrDefault();

            // if no available appointment was found, it means somebody must have just booked this appointment
            if (appointmentToAdd == null)
            {
                // fill data that should be shown in view if model validation returns errors
                DateTime checkDate = DateTime.Now.Date.AddDays(1);
                var availableAppointments = dbState.Appointments.Where(s => s.DoctorId == doctorId).Where(s => s.IsAvailable == true).Where(s => s.AppointmentDateTime >= checkDate).Select(s => s.AppointmentDateTime);
                var availableAppointmentDates = new List<DateTime>();
                var availableAppointmentDatesAsString = new List<string>();
                foreach (var availableAppointment in availableAppointments)
                {
                    if (!availableAppointmentDates.Contains(availableAppointment.Date))
                    {
                        availableAppointmentDates.Add(availableAppointment.Date);
                        availableAppointmentDatesAsString.Add(string.Concat(availableAppointment.Month, "/", availableAppointment.Day, "/", availableAppointment.Year));
                    }
                }

                appointment.AvailableDatesString = availableAppointmentDatesAsString;
                appointment.AvailableHoursString = new List<string>();
                ModelState.AddModelError("", "Przykro nam, ktoś właśnie dokonał rezerwacji w wybranym terminie.");
                return View(appointment);
            }
            else
            {
                // if a free appointment was found in selected date and time, we change it to not available anymore and set patient id
                appointmentToAdd.IsAvailable = false;
                appointmentToAdd.PatientId = patientId;
                dbState.Entry(appointmentToAdd).State = EntityState.Modified;
                dbState.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        [Authorize]
        public ActionResult ChooseDoctor()
        {
            List<DoctorsForScheduleViewModel> doctorsFullNamesList = new List<DoctorsForScheduleViewModel>();
            foreach (var doctor in dbState.Doctors)
            {
                DoctorsForScheduleViewModel doctorFullName = new DoctorsForScheduleViewModel { DoctorId = doctor.DoctorId, DoctorFullName = string.Concat(doctor.DoctorFirstName, " ", doctor.DoctorLastName) };
                doctorsFullNamesList.Add(doctorFullName);
            }

            ViewBag.DoctorsList = new SelectList(doctorsFullNamesList, "DoctorId", "DoctorFullName");
            return View();
        }

        [HttpPost]
        public ActionResult ChooseDoctor(DoctorsForScheduleViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.DoctorId = (Request.Form["DoctorsList"] == "") ? 0 : int.Parse(Request.Form["DoctorsList"]);
                if (model.DoctorId == 0)
                {
                    ModelState.AddModelError("", "Nie wybrano żadnego lekarza");

                    List<DoctorsForScheduleViewModel> doctorsFullNamesList = new List<DoctorsForScheduleViewModel>();
                    foreach (var doctor in dbState.Doctors)
                    {
                        DoctorsForScheduleViewModel doctorFullName = new DoctorsForScheduleViewModel { DoctorId = doctor.DoctorId, DoctorFullName = string.Concat(doctor.DoctorFirstName, " ", doctor.DoctorLastName) };
                        doctorsFullNamesList.Add(doctorFullName);
                    }

                    ViewBag.DoctorsList = new SelectList(doctorsFullNamesList, "DoctorId", "DoctorFullName");
                    return View(model);
                }
                return RedirectToAction("Create", "Appointments", new { doctorId = model.DoctorId });
            }
            else
                return View();
        }

        [HttpGet]
        public JsonResult GetAvailableHours(int selectedDoctorId, DateTime selectedDate)
        {
            var appointmentsFromDb = dbState.Appointments.Where(s => s.DoctorId == selectedDoctorId).Where(s => s.IsAvailable == true).Where(s => s.AppointmentDateTime >= DateTime.Now);
            var appointmentsWithinSelectedDate = new List<DateTime>();

            // choose only appointment dates equal to selected date
            foreach (var appointment in appointmentsFromDb)
            {
                if (appointment.AppointmentDateTime.Date == selectedDate) appointmentsWithinSelectedDate.Add(appointment.AppointmentDateTime);
            }

            // take only HH:MM from dates above
            var enabledHours = new List<string>();
            foreach (var appointment in appointmentsWithinSelectedDate)
            {
                string appointmentMinutes = appointment.Minute == 0 ? "00" : appointment.Minute.ToString();
                enabledHours.Add(string.Concat(appointment.Hour, ":", appointmentMinutes));
            }

            // convert result into selectlist
            List<SelectListItem> selectListToReturn = new List<SelectListItem>();
            foreach (var hour in enabledHours)
            {
                selectListToReturn.Add(new SelectListItem { Text = hour, Value = hour });
            }

            // return result as json
            return Json(selectListToReturn, JsonRequestBehavior.AllowGet);
        }

        // GET: Appointments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appointments appointments = dbState.Appointments.Find(id);
            if (appointments == null)
            {
                return HttpNotFound();
            }
            return View(appointments);
        }

        // POST: Appointments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Appointments appointmentToBeReleased = dbState.Appointments.Find(id);
            appointmentToBeReleased.IsAvailable = true;
            appointmentToBeReleased.PatientId = null;
            dbState.Entry(appointmentToBeReleased).State = EntityState.Modified;
            dbState.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Appointments
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult SetUpSchedule()
        {
            List<DoctorsForScheduleViewModel> DoctorsFullNamesList = new List<DoctorsForScheduleViewModel>();
            foreach (var doctor in dbState.Doctors)
            {
                DoctorsForScheduleViewModel doctorFullName = new DoctorsForScheduleViewModel { DoctorId = doctor.DoctorId, DoctorFullName = string.Concat(doctor.DoctorFirstName, " ", doctor.DoctorLastName) };
                DoctorsFullNamesList.Add(doctorFullName);
            }
            ViewBag.DoctorsList = new SelectList(DoctorsFullNamesList, "DoctorId", "DoctorFullName");

            return View();
        }

        [HttpPost]
        public ActionResult SetUpSchedule(SetUpScheduleViewModel scheduleModel)
        {
            if (ModelState.IsValid)
            {
                if (scheduleModel.DateFrom > scheduleModel.DateTo)
                {
                    ModelState.AddModelError("", "Data końca nie może być mniejsza niż data początku!");
                    return View(scheduleModel);
                }

                scheduleModel.DoctorId = (Request.Form["DoctorsList"] == "") ? 0 : int.Parse(Request.Form["DoctorsList"]);
                var doctorsAvailavbilityList = dbState.DoctorsAvailability.Where(s => s.DoctorId == scheduleModel.DoctorId).ToList();

                // fill only dates from user-defined range that remain empty in database = fill every date from range except dates already existing in database
                FillGapsWithDates(scheduleModel.DateFrom, scheduleModel.DateTo, doctorsAvailavbilityList);
                dbState.SaveChanges();
                return RedirectToAction("SetUpSchedule", "Appointments");
            }
            return View(scheduleModel);
        }

        private void FillGapsWithDates(DateTime dataSearchStart, DateTime dataSearchEnd, List<DoctorsAvailability> doctorsAvailabilityList)
        {
            List<DateTime> possibleDatesList = new List<DateTime>();
            for (DateTime possibleDate = dataSearchStart; possibleDate <= dataSearchEnd; possibleDate = possibleDate.AddDays(1))
            {
                possibleDatesList.Add(possibleDate);
            }

            int DoctorId = doctorsAvailabilityList.Select(s => s.DoctorId).FirstOrDefault();
            List<DateTime> datesHoursFromDatabaseList = dbState.Appointments.Where(s => s.AppointmentDateTime >= dataSearchStart && s.AppointmentDateTime <= dataSearchEnd && s.DoctorId == DoctorId).Select(p => p.AppointmentDateTime).Distinct().ToList();

            if (datesHoursFromDatabaseList.Count == 0) FillWithDates(dataSearchStart, dataSearchEnd, doctorsAvailabilityList);
            else
            {
                DateTime currentDate;
                foreach (DateTime? datesHoursFromDatabaseObject in datesHoursFromDatabaseList)
                {
                    currentDate = ((DateTime)datesHoursFromDatabaseObject).Date;
                    if (possibleDatesList.Contains(currentDate)) possibleDatesList.Remove(currentDate);
                }
                foreach (DateTime dateToFill in possibleDatesList)
                {
                    FillWithDates(dateToFill, dateToFill, doctorsAvailabilityList);
                }
            }
        }

        private void FillWithDates(DateTime dateStart, DateTime dateEnd, List<DoctorsAvailability> doctorsAvailabilityList)
        {
            for (var currentDay = dateStart; currentDay.Date <= dateEnd; currentDay = currentDay.AddDays(1))
            {
                if ((currentDay.DayOfWeek == DayOfWeek.Saturday) || (currentDay.DayOfWeek == DayOfWeek.Sunday)) continue;
                if (freeDaysList.Contains(currentDay.Date)) continue;
                int DoctorId = doctorsAvailabilityList.Select(s => s.DoctorId).FirstOrDefault();
                var dostepnoscForCurrentDay = doctorsAvailabilityList.Where(s => s.DayOfTheWeek == (int)currentDay.DayOfWeek).FirstOrDefault();
                if (dostepnoscForCurrentDay == null) continue;
                for (var currentDateTimeToAdd = currentDay.AddHours(dostepnoscForCurrentDay.HourFrom); currentDateTimeToAdd <= currentDay.AddHours(dostepnoscForCurrentDay.HourTo).AddHours(-0.5); currentDateTimeToAdd = currentDateTimeToAdd.AddHours(0.5))
                {
                    var AppointmentDateTimeToAdd = new Appointments { DoctorId = DoctorId, IsAvailable = true, PatientId = null, AppointmentDateTime = currentDateTimeToAdd };
                    dbState.Appointments.Add(AppointmentDateTimeToAdd);
                }
            }
        }

        private void SetPatientIdIfNeeded()
        {
            if (patientId == 0)
            {
                string patientEmail = User.Identity.GetUserName();
                patientId = dbState.Patients.Where(p => p.Email == patientEmail).FirstOrDefault().PatientId;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) dbState.Dispose();
            base.Dispose(disposing);
        }
    }
}