﻿using Stomatologia.Models;
using System.Web.Mvc;

namespace Stomatologia.Controllers
{
    public class PatientsController : Controller
    {
        private StomatologyEntities db = new StomatologyEntities();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PatientId,PatientFirstName,PatientLastName,PatientPESEL,Email,PhoneNumber")] Patients patients)
        {
            if (ModelState.IsValid)
            {
                db.Patients.Add(patients);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            return View(patients);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}