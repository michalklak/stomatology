﻿using System.Web.Mvc;

namespace Stomatologia.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Strona kontaktowa";

            return View();
        }

        public ActionResult Pricing()
        {
            ViewBag.Message = "Cennik naszych usług";

            return View();
        }

        public ActionResult DoctorsOverview()
        {
            ViewBag.Message = "Nasi lekarze";

            return View();
        }
    }
}