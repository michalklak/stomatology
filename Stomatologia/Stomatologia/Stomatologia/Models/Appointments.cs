//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Stomatologia.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Appointments
    {
        public int AppointmentId { get; set; }

        [Display(Name = "Termin wizyty")]
        public System.DateTime AppointmentDateTime { get; set; }

        [Display(Name = "ID lekarza")]
        public int DoctorId { get; set; }

        [Display(Name = "Status dostępności wizyty")]
        public bool IsAvailable { get; set; }

        [Display(Name = "ID Pacjenta")]
        public Nullable<int> PatientId { get; set; }
    
        public virtual Doctors Doctors { get; set; }
        public virtual Patients Patients { get; set; }
    }
}
