﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stomatologia.Models
{
    public class DoctorsForScheduleViewModel
    {
        public int DoctorId { get; set; }
        public string DoctorFullName { get; set; }
    }
}