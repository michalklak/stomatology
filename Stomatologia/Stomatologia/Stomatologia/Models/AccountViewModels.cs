﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Stomatologia.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "E-mail")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(100, ErrorMessage = "E-mail może zawierać maksymalnie 100 znaków.")]
        [EmailAddress(ErrorMessage = "Wprowadzono niepoprawny format adresu Email")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} musi zawierać minimum {2} znaków.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Potwierdź hasło")]
        [Compare("Password", ErrorMessage = "Podane hasła nie są zgodne.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "{0} może zawierać maksymalnie 40 znaków.")]
        [Display(Name = "Imię")]
        [RegularExpression(@"^[a-zA-ZżźćńółęąśŻŹĆĄŚĘŁÓŃ]+$", ErrorMessage = "Imię nie może zawierać cyfr ani znaków specjalnych.")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(60, ErrorMessage = "{0} może zawierać maksymalnie 60 znaków.")]
        [Display(Name = "Nazwisko")]
        [RegularExpression(@"^[-a-zA-ZżźćńółęąśŻŹĆĄŚĘŁÓŃ]+$", ErrorMessage = "{0} nie może zawierać cyfr ani znaków specjalnych poza myślnikiem.")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "PESEL")]
        [StringLength(11, ErrorMessage = "PESEL musi liczyć dokładnie 11 cyfr", MinimumLength = 11)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "PESEL musi składać się z samych cyfr")]
        public string PESEL { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Nr telefonu")]
        [StringLength(25, ErrorMessage = "{0} może liczyć maksymalnie 25 cyfr")]
        [MinLength(7, ErrorMessage = "{0} musi liczyć minimum 7 cyfr")]
        public string PhoneNumber { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
