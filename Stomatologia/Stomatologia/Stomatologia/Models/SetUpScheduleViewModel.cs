﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Stomatologia.Models
{
    public class SetUpScheduleViewModel
    {
        [Required]
        [Display(Name = "Lekarz")]
        public int DoctorId { get; set; }

        [Required]
        [Display(Name = "Data od")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime DateFrom { get; set; }

        [Required]
        [Display(Name = "Data do")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime DateTo { get; set; }
    }
}