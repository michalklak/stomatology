﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Stomatologia.Models
{
    public class NewAppointmentViewModel
    {
        public int AppointmentId { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime AppointmentDate { get; set; }

        public string AppointmentTime { get; set; }

        public int DoctorId { get; set; }

        public List<string> AvailableDatesString { get; set; }

        public List<string> AvailableHoursString { get; set; }
    }
}